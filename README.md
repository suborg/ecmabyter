# EcmaByter - world's smallest BytePusher engine for browsers

## About

This is an ECMAScript 6 implementation of [BytePusher](https://esolangs.org/wiki/BytePusher) specification. But with a small difference - this engine is really small, exactly 999 bytes long. All this with remappable keys support (no touch controls though), 256x256 graphics and sound.

## Usage

The engine library exposes 3 parameters to start the VM:

```
EcmaByter(global, config, rom)
```

- `global` is the `window` object. You need to pass it explicitly as the first parameter. That was done to enable EcmaByter to function as part of some deeply structured modules.
- `config` is an object with two possible keys: `canvas` is mandatory (reference to a Canvas object with `width=256` and `height=256` attributes), and `keys` is optional (array of 16 key symbols (like `Enter`, `a`, `5`, `Backspace` etc - `KeyEvent.key` format compatible) that correspond to the 0 to F digit mapping). Default key map is straight digits and `a` to `f` keys.
- `rom` must be an ArrayBuffer with BytePusher ROM binary.

## Demos

- [Sine scroller](http://suborg.gitlab.io/ecmabyter/) (default)
- [Keypad test](http://suborg.gitlab.io/ecmabyter/?bp/keyboard.bp)
- [Audio test](http://suborg.gitlab.io/ecmabyter/?bp/audio.bp)
- [Palette test](http://suborg.gitlab.io/ecmabyter/?bp/palette.bp)
- [Sprite test](http://suborg.gitlab.io/ecmabyter/?bp/sprites.bp)

## Compatibility

All ROMs from the official page at Esolangs work fine. Due to key state XORring and async input nature in JavaScript, multiple keypresses at the same time may work incorrectly. Also, input events are bound to global object with `onkeyup` and `onkeydown` handlers, so please keep that in mind.

Of course, no polyfills are used but the library should work fine with all modern browser engines supporting Web Audio API.

## Credits

- Spec: [Javamannen](https://esolangs.org/wiki/User:Javamannen)
- Inspiration: [Nucular](http://nucular.github.io)
- Implementation: [Plugnburn](https://esolangs.org/wiki/User:Plugnburn) aka [Luxferre](https://luxferre.top) aka [@suborg](https://gitlab.com/suborg/)
